variable "domain" {
  type        = string
  description = "The name of the Domain."
}

variable "namecheap_user_name" {
  type        = string
  description = "The username of the Domain."
}

variable "namecheap_api_user" {
  type        = string
  description = "The api user of the domain account."
}

variable "namecheap_api_key" {
  type        = string
  description = "The api token of the domain account."
}

variable "namecheap_client_ip" {
  type        = string
  description = "The IP address that belongs to client."
}
