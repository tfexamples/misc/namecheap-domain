resource "namecheap_domain_records" "my-ns" {
  domain = var.domain
  mode   = "OVERWRITE"

  nameservers = [
    "ns-cloud-b1.googledomains.com.",
    "ns-cloud-b2.googledomains.com.",
  ]
}
